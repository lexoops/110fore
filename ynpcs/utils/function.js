function o(o, n, e, i) {
    t(o, n, e, "POST", i);
}

function post(url, parmas, callBack, showLoading) {
    t(url, parmas, callBack, "POST", showLoading);
}

function get(url, parmas, callBack, showLoading) {
    t(url, parmas, callBack, "GET", showLoading);
}

function n(o, n, e, i) {
    t(o, n, e, "GET", i);
}

function t(o, n, callback, i, a) {
    var r = {
        url: o,
        json: n,
        method: i,
        loading: a
    }, l = u.sessionid;

    if ("" != l && null != l) {
        d = {
            "content-type": "application/json",
            Cookie: "PHPSESSID=" + l
        };
        if (null != getApp().globalData.token) {
            d = {
                // "content-type": "application/x-www-form-urlencoded",
                Cookie: "PHPSESSID=" + l,
                "token":getApp().globalData.token
            };
        }
    }
    else {
        var d = {
            // "content-type": "application/json"
        };
        if (null != getApp().globalData.token) {
            d = {
                // "content-type": "application/x-www-form-urlencoded",
                "token":getApp().globalData.token
            };
        }
    }
    // if (o.indexOf("https://") < 0 && (o = c.host + o), "POST" == i)
    //     f = s(n);
    // else
    if (o.indexOf("http") === -1) {
        o = c.host + o;
    }
    var f = n;
    var url =o;
    "" != a && null != a || wx.showNavigationBarLoading(), wx.request({
        url: o,
        data: f,
        header: d,
        method: i,
        success: function(o) {
            console.log(   "请求:",url ,"凭据:" ,f , "请求头:", d, "方式:" ,i , "函数：", callback)
            console.log("返回:", o);
            "" != o.data.sessionid && null != o.data.sessionid && (wx.setStorageSync("sessionid", o.data.sessionid), 
            u.sessionid = o.data.sessionid, o.data.source && (u.globalData.source = o.data.source));
            // if (o.data.code === "0") {
                // 成功
            callback(o.data);
            // }
            // else {
            //     wx.showToast({
            //         title: o.data.msg,
            //         image: "/utils/icon/error.png"
            //     });
            // }
            //console.log(o),
            // e(o, r, t);

        },
        fail: function(e) {
            console.log(   "请求失败:", e , "请求:",url, "方式:" ,i, "参数:" ,f)
        }
    });
}

function e(o, n, e) {
    var s = o.data.status;
    c = o.data.userinfo;
    "" != s && null != s ? (1 == s && (wx.hideToast(), wx.hideNavigationBarLoading(), 
    "" != c && null != c && ("login" == c && i(), "more" == c && a(), "force" == c && userforce()), 
    "function" == typeof e && e(o.data)), 2 == s && i(function() {
        n.url && t(n.url, n.json, e, n.method, n.loading);
    }), 3 == s && wx.showToast({
        title: "没有内容了",
        image: "/utils/icon/error.png"
    }), 5 == s && wx.showModal({
        title: "温馨提示",
        showCancel: !1,
        content: o.data.info,
        success: function(o) {
            o.confirm && wx.reLaunch({
                url: "../index/index"
            });
        }
    }), 4 == s && (wx.showToast({
        title: "没有内容了",
        image: "/utils/icon/error.png"
    }), setTimeout(function() {
        wx.navigateBack({
            delta: 1
        });
    }, 1500))) : wx.showToast({
        title: "连接失败",
        image: "/utils/icon/error.png"
    });
}

function i(v) {
    wx.login({
        success: function(t) {
            var e = {
                code: t.code
            };
            o(c.loginUrl, e, v, false);
        }
    });
}

function s(o) {
    return Object.keys(o).map(function(n) {
        return encodeURIComponent(n) + "=" + encodeURIComponent(o[n]);
    }).join("&");
}

function a(t) {
    wx.login({
        success: function(e) {
            wx.getUserInfo({
                lang: "zh_CN",
                success: function(n) {
                    var i = {
                        code: e.code,
                        encryptedData: n.encryptedData,
                        iv: n.iv,
                        source: u.globalData.source
                    };
                    o(c.loginUrl, i, t, !0);
                },
                fail: function() {
                    var o = {
                        code: e.code,
                        source: u.globalData.source
                    };
                    n(c.loginUrl, o, t, !0);
                }
            });
        }
    });
}

function upFileForm(url,formdata, callback) {
    fetch( url,
        {
            method:"POST",
            body:formdata
        }
        ).then(status).then(resultJSON).then(
            console.log(status,resultJSON)
    )

}

var c = require("./config.js"), u = getApp();

module.exports = {
    request_post: post,
    request_get: get,
    userlogin: i,
    uploadFormdata:upFileForm,
    statusBarHeight: function(o) {
        wx.getSystemInfo({
            success: function(n) {
                o(n.statusBarHeight + "px");
            }
        });
    },
    fail_toast: function(o) {
        wx.showToast({
            title: o,
            duration: 2e3,
            image: "/utils/icon/error.png"
        });
    },
    success_toast: function(o) {
        wx.showToast({
            title: o,
            duration: 2e3
        });
    },
    loading: function(o) {
        wx.showLoading({
            title: o
        });
    }
};