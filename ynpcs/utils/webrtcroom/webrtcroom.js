Component({
    properties: {
        hangUp: {
            type: Boolean,
            value: !0
        },
        beauty: {
            type: Number,
            value: 5
        },
        aspect: {
            type: String,
            value: "9:16"
        },
        muted: {
            type: Boolean,
            value: !1
        },
        waitingImage: {
            type: "String",
            value: ""
        },
        backgroundMute: {
            type: Boolean,
            value: "true"
        },
        debug: {
            type: Boolean,
            value: !1
        }
    },
    data: {
        self: {},
        roomsigfailcount: 0,
        maxmembers: 3,
        members: {},
        firstuserid: ""
    },
    ready: function() {
        var e = this;
        self = this, wx.getSystemInfo({
            success: function(s) {
                e.setData({
                    top: parseInt(s.statusBarHeight) + "px"
                });
            }
        });
    },
    methods: {
        config: function(e) {
            self.data.sdkappid = e.sdkappid, self.data.userid = e.userid, self.data.usersig = e.usersig, 
            self.data.privmapencrypt = e.privmapencrypt, self.data.roomid = e.roomid;
        },
        start: function(e) {
            wx.setKeepScreenOn({
                keepScreenOn: !0
            }), self.post_debug("开启视频通话");
            var s = {
                sdkappid: self.data.sdkappid,
                userid: self.data.userid,
                usersig: self.data.usersig,
                privmapencrypt: self.data.privmapencrypt,
                roomid: self.data.roomid
            };
            self.post_debug("获取标签信息：", s);
            var t = "https://official.opensso.tencent-cloud.com/v4/openim/jsonvideoapp?";
            t = t + "sdkappid=" + s.sdkappid + "&identifier=" + s.userid + "&usersig=" + s.usersig, 
            t += "&random=9999&contenttype=json", self.post_debug("roomsig请求地址：", t);
            var o = {
                Cmd: 1,
                SeqNo: 1,
                BusType: 7,
                GroupId: s.roomid
            }, a = {
                PrivMapEncrypt: s.privmapencrypt,
                TerminalType: 1,
                FromType: 3,
                SdkVersion: 26280566
            };
            console.log("reqHead:"), console.log(o), console.log("reqBody:"), console.log(a), 
            wx.request({
                url: t,
                data: {
                    ReqHead: o,
                    ReqBody: a
                },
                method: "POST",
                success: function(t) {
                    if (self.post_debug("roomsig内容：", t), 0 == t.data.RspHead.ErrorCode) {
                        var o = encodeURIComponent(JSON.stringify(t.data.RspBody)), a = "room://cloud.tencent.com?sdkappid=" + s.sdkappid + "&roomid=" + s.roomid + "&";
                        a = a + "userid=" + s.userid + "&roomsig=" + o, self.post_debug("推流地址：", a), self.setData({
                            pushURL: a
                        }, function() {
                            self.data.pusherContext || (self.data.pusherContext = wx.createLivePusherContext("rtcpusher")), 
                            self.data.pusherContext.start(), "function" == typeof e && e(self.data.pusherContext);
                        });
                    }
                    0 != t.data.RspHead.ErrorCode && (self.data.roomsigfailcount++, self.data.roomsigfailcount > 3 ? self.bindRoomEvent("获取房间SIG错误", 2) : (self.post_debug("获取房间sig失败[" + self.data.roomsigfailcount + "]次，重试~"), 
                    setTimeout(function() {
                        self.start(e);
                    }, 2e3)));
                },
                fail: function() {
                    self.data.roomsigfailcount++, self.data.roomsigfailcount > 3 ? self.bindRoomEvent("获取房间SIG错误", 2) : (self.post_debug("获取房间sig失败[" + self.data.roomsigfailcount + "]次，重试~"), 
                    setTimeout(function() {
                        self.start(e);
                    }, 2e3));
                }
            });
        },
        onPush: function(e) {
            self.post_debug("推流回调");
            var s;
            switch (s = e.detail ? e.detail.code : e, self.post_debug("", e), s) {
              case 1002:
                self.post_debug("推流成功：", s);
                break;

              case -1301:
                self.post_debug("打开摄像头失败:", s), self.stop(), self.bindRoomEvent("打开摄像头失败", 2);
                break;

              case -1302:
                self.post_debug("打开麦克风失败:", s), self.stop(), self.bindRoomEvent("打开摄像头失败", 2);
                break;

              case -1307:
                self.post_debug("推流连接断开:", s), self.stop(), self.bindRoomEvent("推流连接断开", 2);
                break;

              case 5e3:
                self.post_debug("收到5000:", s), self.stop(), self.bindRoomEvent("收到5000", 2);
                break;

              case 1018:
                self.post_debug("进房成功:", s), self.bindRoomEvent("进房成功", 1);
                break;

              case 1019:
                self.post_debug("加入房间异常:", s), self.stop(), self.bindRoomEvent("加入房间异常，建议重试", 3);
                break;

              case 1020:
                self.post_debug("获得成员列表:", s), self.onWebRTCUserListPush(e.detail.message), self.bindRoomEvent("获取对方推流成功", 1);
                break;

              case 1021:
                self.post_debug("网络类型发生变化，需要重新进房:", s), self.stop(), self.start();
                break;

              default:
                self.post_debug("其他推流情况:", s);
            }
        },
        onWebRTCUserListPush: function(e) {
            self.post_debug("将成员列表加入通话中", e);
            var s = JSON.parse(e);
            if (s) {
                var t = s.userlist;
                if (!t || t.length < 1) self.firstuserid && self.bindRoomEvent("对方挂断视频通话", 1e3); else {
                    self.post_debug("获取列表成功：", t);
                    var o = [];
                    t && t.forEach(function(e) {
                        var s = {
                            userid: e.userid,
                            playurl: e.playurl
                        };
                        o.push(s);
                    }), self.post_debug("来源视频列表：", o), self.data.members = o;
                    var a = self.firstmembers();
                    console.log("first="), console.log(a), a && (self.firstuserid = a.userid, self.setData({
                        firstURL: a
                    }));
                }
            }
        },
        firstmembers: function() {
            var e = "", s = self.data.members;
            return self.post_debug("pushers:", s), Object.keys(s).length > 0 && (e = {
                userid: s[0].userid,
                playurl: s[0].playurl
            }), self.post_debug("first:", e), e;
        },
        othermembers: function() {
            var e = [], s = self.data.members;
            self.post_debug("othersPushers:", s);
            var t = Object.keys(s).length;
            if (t > 1) for (var o = 1; o < t; o++) {
                var a = {
                    userid: s[o].userid,
                    playurl: s[o].playurl
                };
                e.push(a);
            }
            return self.post_debug("others:", e), e;
        },
        stop: function() {
            self.post_debug("停止推流和通话"), self.data.pusherContext || (self.data.pusherContext = wx.createLivePusherContext("rtcpusher")), 
            self.data.pusherContext && self.data.pusherContext.stop();
            var e = wx.createLivePlayerContext("rtcplayer");
            e && e.stop();
        },
        pause: function() {
            self.post_debug("暂停推流"), self.data.pusherContext || (self.data.pusherContext = wx.createLivePusherContext("rtcpusher")), 
            self.data.pusherContext && self.data.pusherContext.pause();
            var e = wx.createLivePlayerContext("rtcplayer");
            e && e.pause();
        },
        resume: function() {
            self.post_debug("恢复推流"), self.data.pusherContext || (self.data.pusherContext = wx.createLivePusherContext("rtcpusher")), 
            self.data.pusherContext && self.data.pusherContext.resume();
            var e = wx.createLivePlayerContext("rtcplayer");
            e && e.resume();
        },
        muted: function() {
            self.post_debug("静音播放"), self.data.muted ? (wx.showToast({
                title: "已恢复声音",
                duration: 2e3
            }), self.data.muted = !1, self.setData({
                muted: !1
            })) : (self.data.muted = !0, self.setData({
                muted: !0
            }), wx.showToast({
                title: "已静音"
            })), console.log(self.data.muted);
        },
        switchCamera: function() {
            self.post_debug("切换摄像头"), self.data.pusherContext || (self.data.pusherContext = wx.createLivePusherContext("rtcpusher")), 
            self.data.pusherContext && self.data.pusherContext.switchCamera({});
        },
        bindRoomEvent: function(e, s) {
            s || (s = 1), self.triggerEvent("RoomEvent", {
                info: e,
                code: s
            }, {});
        },
        PlayerState: function(e) {
            self.triggerEvent("PlayerState", {
                e: e
            }, {});
        },
        hangCall: function(e) {
            self.triggerEvent("hangCall", {
                e: e
            }, {});
        },
        openToClose: function(e) {
            self.setData({
                hiddenWait: e
            });
        },
        post_debug: function(e, s) {
            self.data.debug && (e && console.log(e), s && console.log(s));
        }
    }
});