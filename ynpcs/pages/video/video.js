var t = require("../../utils/function.js"), o = (getApp(), "");

Page({
    data: {
        isthrough: !1,
        out: "",
        mutedstate: 0,
        stateheartbeat: !0,
        roomid: 0
    },
    onLoad: function(t) {
        (o = this).data.mutedstate = t.state;
        for (var e = [ "scope.userLocation", "scope.record", "scope.camera" ], n = 0; n < e.length; n++) !function(t) {
            wx.authorize({
                scope: e[t],
                complete: function() {
                    2 == t && o.is_authorize();
                }
            });
        }(n);
    },
    is_authorize: function() {
        wx.getSetting({
            complete: function(t) {
                t.authSetting["scope.camera"] && t.authSetting["scope.record"] && t.authSetting["scope.userLocation"] ? o.start() : wx.showModal({
                    title: "请允许使用您的",
                    content: "地理位置、录音、摄像头等功能",
                    showCancel: !1,
                    success: function(t) {
                        wx.reLaunch({
                            url: "../index/index?userauth=1"
                        });
                    }
                });
            }
        });
    },
    onHide: function() {
        console.log("页面隐藏挂断电话"), o.stop(), wx.reLaunch({
            url: "../index/index"
        });
    },
    heartbeat: function() {
        function e() {
            console.log("===========");
            wx.getLocation({
                type: "gcj02",
                success: function(e) {
                    var n = {
                        lat: e.latitude,
                        lng: e.longitude,
                        roomid: o.data.roomid
                    };
                    t.request_get("/Callpolice/main/heartbeat", n, function(t) {
                        console.log(t);
                    });
                }
            });
        }
        console.log("启动心跳包------------------"), e(), o.data.out = setInterval(function() {
            o.data.stateheartbeat ? e() : o.stop();
        }, 3e4);
    },
    closeheartbeat: function() {
        console.log("关闭心跳包"), o.data.stateheartbeat = !1, clearInterval(o.data.out);
        var e = {
            roomid: o.data.roomid
        };
        t.request_get("/Callpolice/main/hangup", e, function(t) {
            console.log(t);
        });
    },
    start: function(e) {
        wx.getLocation({
            type: "gcj02",
            success: function(e) {
                var n = {
                    lat: e.latitude,
                    lng: e.longitude
                };
                t.request_post("/Callpolice/main/call", n, function(t) {
                    o.data.roomid = t.roomid, o.data.stateheartbeat ? (o.data.webrtcroomComponent = o.selectComponent("#webrtcroom"), 
                    o.data.webrtcroomComponent.config(t), o.data.webrtcroomComponent.start(), o.heartbeat()) : o.stop();
                });
            }
        });
    },
    pause: function(t) {
        o.data.webrtcroomComponent = o.selectComponent("#webrtcroom"), o.data.webrtcroomComponent.pause();
    },
    resume: function(t) {
        o.data.webrtcroomComponent = o.selectComponent("#webrtcroom"), o.data.webrtcroomComponent.resume();
    },
    stop: function(t) {
        o.data.webrtcroomComponent = o.selectComponent("#webrtcroom"), o.data.webrtcroomComponent.stop(), 
        o.closeheartbeat();
    },
    muted: function(t) {
        o.data.webrtcroomComponent = o.selectComponent("#webrtcroom"), o.data.webrtcroomComponent.muted();
    },
    switchcamera: function(t) {
        o.data.webrtcroomComponent = o.selectComponent("#webrtcroom"), o.data.webrtcroomComponent.switchCamera();
    },
    hangCall: function(t) {
        var e = "视频通话中，您确认挂断吗?";
        this.data.isthrough || (e = "视频连接中，您确认挂断吗？"), wx.showModal({
            title: "挂断提示",
            content: e,
            success: function(t) {
                t.confirm && (o.stop(), wx.reLaunch({
                    url: "../index/index"
                }));
            }
        });
    },
    roomevent: function(t) {
        1e3 == t.detail.code && (o.stop(), wx.showModal({
            title: "视频已挂断",
            content: "如有需要请重新拨打视频报警",
            showCancel: !1,
            success: function(t) {
                t.confirm && wx.reLaunch({
                    url: "../index/index"
                });
            }
        }));
    },
    PlayerState: function(t) {
        2004 == t.detail.e.detail.code && (o.data.webrtcroomComponent = o.selectComponent("#webrtcroom"), 
        o.data.webrtcroomComponent.openToClose(!0), this.setData({
            isthrough: !0
        }));
    },
    onReady: function() {
        1 == o.data.mutedstate && (o.muted(), wx.hideToast());
    },
    onUnload: function() {
        console.log("页面卸载，关闭通话"), o.stop();
    },
    onShareAppMessage: function() {
        return {
            imageUrl: "https://www.3bridgetec.cn:8443/api/img/gyshare.png",
            path: "/pages/index/index"
        };
    }
});