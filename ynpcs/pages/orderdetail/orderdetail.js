var e = require("../../utils/function.js"), t = "", a = 0;

Page({
    data: {
        isFinish: !1,
        lat: "",
        lng: ""
    },
    onLoad: function(i) {
        t = this, a = i.id, wx.getLocation({
            success: function(i) {
                var n = {
                    id: a,
                    lat: i.latitude,
                    lng: i.longitude
                };
                //e.request_post("/Callpolice/view/info", n, t.result);
            },
            fail: function() {
                var i = {
                    id: a,
                    lat: "",
                    lng: ""
                };
                //e.request_post("/Callpolice/view/info", i, t.result);
            }
        });
    },
    result: function(a) {
        console.log(a), 1 == a.state ? t.setData({
            realname: a.realname,
            phone: a.phone,
            addree: a.addree,
            readtime: a.readtime,
            police_name: a.police_name,
            suaddree: a.suaddree,
            content: a.content,
            video: a.video,
            policevideo: a.policevideo,
            lat: a.lat,
            lng: a.lng,
            isFinish: !0,
            bgsrc: a.bg
        }) : 2 == a.state ? e.fail_toast(a.info) : 3 == a.state && (e.fail_toast(a.info), 
        setTimeout(function() {
            wx.redirectTo({
                url: "../login/login"
            });
        }, 1500));
    },
    callNumber: function() {
        wx.makePhoneCall({
            phoneNumber: t.data.phone + ""
        });
    },
    callPoliceAddress: function() {
        wx.openLocation({
            latitude: parseFloat(t.data.lat),
            longitude: parseFloat(t.data.lng),
            name: "报警人位置",
            address: t.data.addree
        });
    },
    calledAddress: function() {
        wx.openLocation({
            latitude: parseFloat(t.data.lat),
            longitude: parseFloat(t.data.lng),
            name: "报警人位置",
            address: t.data.suaddree
        });
    },
    onReady: function() {
        e.statusBarHeight(t.bar_height);
        var a = wx.createSelectorQuery();
        a.select("#topbar").boundingClientRect(), a.exec(function(e) {
            t.setData({
                list_mar_top: e[0].height + "px"
            });
        });
    },
    bar_height: function(e) {
        t.setData({
            padtop: e
        });
    },
    onShareAppMessage: function() {
        return {
            imageUrl: "https://www.3bridgetec.cn:8443/api/img/policeInfo.png",
            path: "/pages/orderdetail/orderdetail?id=" + a
        };
    }
});