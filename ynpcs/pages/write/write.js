var t = require("../../utils/function.js"), e = require("../../utils/config.js"), a = [], i = "", l = "", s = "", o = "", n = !0, u = !1,
    mypicList ="",
    videoList ="",
    imgCount =0,
    useraddr ="";

Page({

    data: {
        picList: "",
        videoSrc: null,
        name: "",
        tell: "",
        details: "",
        hiddenToast: !0,
        tellValue: "",
        nameValue: ""
    },
    onLoad: function(e) {
        var a = this;
        wx.getSystemInfo({
            success: function(t) {
                a.setData({
                    margintop: t.statusBarHeight + parseInt(.07 * t.screenHeight) + "px",
                    top: parseInt(t.statusBarHeight) + parseInt(.01 * t.screenHeight) + "px",
                    height: t.statusBarHeight + "px"
                });
            }
        }), wx.getLocation({
            type: "gcj02",
            success: function(e) {
                s = e.longitude;
                var i = {
                    lat: o = e.latitude,
                    lng: s
                };
                //a.getLocaltion(o, s);
                //console.log(i), t.request_post("", i, a.result);
            },
            fail: function() {
                u ? wx.reLaunch({
                    url: "../index/index"
                }) : wx.openSetting({
                    "scope":"scope.userLocation"
                }), u = !0;
            }
        });
    },
    onShow: function() {},
    result: function(t) {
        console.log(t), this.setData({
            tellValue: t.phone,
            tell: t.phone,
            nameValue: t.realname,
            name: t.realname
        });
    },
    inputName: function(t) {
        this.setData({
            name: t.detail.value
        });
    },
    inputTell: function(t) {
        this.setData({
            tell: t.detail.value
        }), 11 == t.detail.value.length && (/^1[34578]\d{9}$/.test(t.detail.value) ? this.setData({
            detailsfocus: !0
        }) : wx.showToast({
            title: "电话号码错误",
            duration: 2e3,
            image: "/utils/icon/error.png"
        }));
    },
    inputDetails: function(t) {
        this.setData({
            details: t.detail.value
        });
    },
    choicePhoto: function() {
        var t = this, e = 9 - a.length;
        wx.chooseImage({
            count: e,
            sizeType: [ "original", "compressed" ],
            success: function(e) {
                for (var i = 0; i < e.tempFilePaths.length; i++) a.push(e.tempFilePaths[i]);
                t.setData({
                    picList: a
                });
            }
        });
    },
    choiceVideo: function() {
        var t = this;
        wx.chooseVideo({
            compressed: !0,
            success: function(e) {
                t.setData({
                    videoSrc: e.tempFilePath
                });
            }
        });
    },
    deletePic: function(t) {
        var e = t.currentTarget.dataset.idx;
        a.splice(e, 1), this.setData({
            picList: a
        });
    },
    submitData: function() {
        self =this;
        if (0 != this.data.name.length && 11 == this.data.tell.length && /^1[34578]\d{9}$/.test(this.data.tell) && 0 != this.data.details.length && n) {
            self.setData(
                {
                    tellValue: self.data.tell,
                    nameValue: self.data.name,
                }
            );
            if (n){
             /*   wx.showLoading({
                    title: "提交中..."
                })*/
             self.showLoading("提交中...")
            };
            if ( 0 != a.length)
            {
               this.uploadPic();
            }


        } else this.checkData();
    },
    showLoading:function(context){
        wx.showToast({
            title: context,
            icon:"loading"
        });
    },
    cancelLoading:function(){
        wx.hideToast();
    },
    uploadPic: function() {
        var t = this;
        i = [];
        var newfile =a;
        for (var count = 0; count < newfile.length; count++) {
             var imageCache =a[count];
             var curtCount =count;
            self =this;
            wx.uploadFile({
                url:  e.host + "/api/uploadImg",
                filePath:imageCache,
                name:"file",
                header:{"token":getApp().globalData.token},

           success:function (response) {
               var res = JSON.parse(response.data);
               if (imgCount==0){
                   mypicList +=res.file.fileUrl;
               } else{
                   mypicList +=("," +res.file.fileUrl);
               }

               if (imgCount== (newfile.length-1)){
                   //console.log(mypicList);
                   if ( null != self.data.videoSrc){
                       self.uploadVideo();
                       //准备上传表单

                   }else {
                       self.uploadData();
                   }
                   imgCount =0;
                   return;
               }
               imgCount ++;
           },
           fail:function (response) {
             console.log(response)
           }
       })
        }

    },
    uploadVideo: function() {
        var self = this;
        wx.uploadFile({
            url: e.host + "/api/uploadVedio",
            filePath: this.data.videoSrc,
            name: "file",
            header:{"token":getApp().globalData.token},
            success: function(e) {
                var a = JSON.parse(e.data);
                l = a.path;
                videoList =a.file.fileUrl;
                //console.log(videoList)
                self.uploadData();
            },
            fail:function (e) {
                console.log(e)
            }
        });
    },
    uploadData: function() {
        //console.log(this.data)
        self =this;
        var e = {
            realname: this.data.name,
            phone: this.data.tell,
            content: this.data.details,
            images: mypicList,
            addree:useraddr,
            video: videoList,
            lng: s,
            lat: o
        };
        t.request_post("/api/submit", e, function (o) {
            self.cancelLoading;
                console.log( e)
                wx.showToast({
                    title: '成功',
                    icon: 'success',
                    duration: 400,
                    success:function(){
                        setTimeout(function () {
                            //要延时执行的代码
                            e ={};
                            a =[];
                            self.data ={};
                            wx.navigateTo({
                                url: '../index/index'
                            });

                        }, 500) //延迟时间
                    }
                });
        },
            true);
    },
    getLocaltion:function(lat, long){
        wx.getLocation({
            type: 'wgs84',
            success: function (res) {
                //2、根据坐标获取当前位置名称，显示在顶部:腾讯地图逆地址解析
                qqmapsdk.reverseGeocoder({
                    location: {
                        latitude: res.latitude,
                        longitude: res.longitude
                    },
                    success: function (addressRes) {
                        var province = addressRes.result.address_component.province;
                        var city = addressRes.result.address_component.city;
                        var cityid=addressRes.result.ad_info.city_code;
                        that.setData({
                            province: province,
                            city: city,
                            cityid:cityid
                        });
                        useraddr =province +city;

                    }
                })

            },
            fail:function(){
                that.setData({
                    province: '定位失败，请选择城市',

                })
            }
        });
},
    uploadResult: function(t) {
        var e = this;
        n = !0, console.log(t), wx.hideLoading(), 1 == t.state ? wx.showModal({
            title: "提示",
            content: "您的信息提交成功！请保持手机畅通",
            showCancel: !1,
            confirmText: "我知道了",
            success: function(t) {
                t.confirm && (wx.reLaunch({
                    url: "../write/write"
                }), e.deleteData());
            }
        }) : wx.showToast({
            title: t.info,
            duration: 2e3,
            image: "/utils/icon/error.png"
        });
    },
    checkData: function() {
        return 0 == this.data.name.length ? (this.showToastMsg("姓名不能为空"), void this.setData({
            namefocus: !0
        })) : 11 == this.data.tell.length && /^1[34578]\d{9}$/.test(this.data.tell) ? 0 == this.data.details.length ? (this.setData({
            detailsfocus: !0
        }), void this.showToastMsg("描述不能为空")) : void 0 : (this.setData({
            tellfocus: !0
        }), void this.showToastMsg("电话号码错误"));
    },
    showToastMsg: function(t) {
        wx.showToast({
            title: t,
            duration: 2e3,
            image: "/utils/icon/error.png"
        });
    },
    backPage: function() {
        wx.reLaunch({
            url: "../index/index"
        });
    },
    nameConfirm: function(t) {
        11 == this.data.tell.length ? this.setData({
            detailsfocus: !0
        }) : this.setData({
            tellfocus: !0
        });
    },
    onPullDownRefresh: function() {},
    onUnload: function() {
        this.deleteData();
    },
    deleteData: function() {
        this.setData({
            picList: "",
            videoSrc: null,
            name: "",
            tell: "",
            details: "",
            hiddenToast: !0,
            tellValue: "",
            nameValue: ""
        }), a = [], i = "", l = "", s = "", o = "", n = !0, u = !1;
    },
    onShareAppMessage: function() {
        return {
            imageUrl: "https://www.3bridgetec.cn:8443/api/img/gyshare.png",
            path: "/pages/index/index"
        };
    }
});