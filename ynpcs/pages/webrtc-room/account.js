//将 TEST_ACCOUNT 的内容替换成"控制台/实时音视频/快速上手第三步"生成的配置文件内容
const TEST_ACCOUNT = { 
  "sdkappid": 1400179054, 
  "users": [
    { 
      "userId": "Miniprogram_trtc_01", 
      "userToken": "eJxlj81Og0AYRfc8BWGLMR8-A9YdoVWLra0KkbqZEBhgaJjBmYFQje9upE0k8W7Pyb25X5qu60a8eb3O8pz3TGF16oih3*oGGFd-sOtogTOFHVH8g2TsqCA4KxURE7QQQjbA3KEFYYqW9GJsKaOd4JXIWqyEyjFYM1kWRzwtnttcAMtfAHLnCq3ORatDuH4OjyxOGF-uE1GZcrngpN2sIBvH-TsMQYLqJvRUuRvkQxOs62Bbf*768b5syo8kMp*ipK-M3aNHYkjz8SXy5d1BvqWcjSmZTSrakss91-fsG9-xZ3QgQlLOJsEGC1m2A78xtG-tB1V8Y*4_" 
      }, 
      { 
        "userId": "Miniprogram_trtc_02", 
        "userToken": "eJxlj0tPg0AYRff8CsIWY2d418QF2hq0JTTaMZXNZGSm*El5dBhL0fjfjbSJJN7tObk390vTdd1YL58uWZbVH5Wiqm*EoV-pBjIu-mDTAKdMUVvyf1AcG5CCsq0ScoDYdV0LobEDXFQKtnA2YqigkXUuWUmVVBlF1khueUGHxVObgxD2p8h1xgrkp6I5ub2PMrFfkmgXchStHnlNgjLtcPia8zTA5kP3Jj*LWR-ushhCmIfHmvDDerbYQ0JeVubNYvL*TCbFZmPG8ZR4Sc8Y6ZK0vfPy69GkglKc7zm*ZwW*7Y-oQcgW6moQLIRdbNnoN4b2rf0A42Fiww__" }, 
        { 
          "userId": "Miniprogram_trtc_03", 
        "userToken": "eJxlj01PhDAARO-8CsJV47alFdbEw36JJLjJLizx1iAtWBSobTElxv9uZDeRxLm*l5nMl*O6rpcl6U1Rlv3QGWpGyT33zvWAd-0HpRSMFob6iv2D3EqhOC0qw9UEISEEATB3BOOdEZW4GE*iE1L1tSpaapQpKfBnsmZvdFo8t2EAYLAEBM8VUZ*LdqdNfNjkO22DIZJIx0OTNltr9u2he1DH8So7fdTJO1Es1OPLMl-FdQFfF3Bl9Xrcr8PEPNo08ROGUVMt4uqIt9lY5xZFJnrG97NJI1p*uYeDWxQGfjCjn1xp0XeTgAAkEPngN57z7fwAC7Ri4Q__" }, 
        { 
          "userId": "Miniprogram_trtc_04", 
        "userToken": "eJxlj81Og0AYRfc8BWFtdBgYUZMuqFBtsbVVEVlN*BmajwozDkNbanx3I20ixrs9J-fmfmq6rhsvD8-nSZbxtlZUdYIZ*o1uIOPsFwoBOU0UtWT*D7K9AMloUigme2gSQjBCQwdyViso4GTMoQYh*VomFVVSZRTZA7nJN7RfPLbZCJnONSJ-FFgfi-zV7dSNeRe*8cCPYJo9RiLtxDIMvVm5vWsn*8n7YeaRMU7DZpG6ME7ncVQH6CkO*IrwZW1C5b0mi1Ydmp1fyo9iE8UX7T0ubXc0GkwqqNjpnu1c4ivHcgZ0y2QDvO4FjExiYgv9xNC*tG9hkWPa" }] }

module.exports = TEST_ACCOUNT;