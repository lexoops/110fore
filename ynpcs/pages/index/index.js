var e = require("../../utils/function.js"), t = getApp(), o = "";
const app = getApp();
const account = require('../webrtc-room/account');


Page({
    data: {
        disabled: false,
        hiddenGetCode: false,
        focusInput: false,
        focusName: false,
        hiddenTell: true,
        code: "",
        name: "",
        hiddenOpenSeting: true,
        opsetingState: 0,

        //tecent clound
        roomNo: '',
        userName: '',
        tapTime: '',
        template: 'bigsmall',  //模板
        users: account.users, //用户
        index: 0 //房间号
    },
    onLoad: function(a) {
        o = this, wx.getSystemInfo({
            success: function(e) {
                o.setData({
                    margintop: e.statusBarHeight + parseInt(.07 * e.screenHeight) + "px"
                });
            }
        });
        1 == a.userauth && (t.globalData.opsetingState = 1, o.setData({
            opsetingState: 1,
            hiddenOpenSeting: false
        }));
        if (!getApp().globalData.needPhone) {
            o.setData({
                hiddenGetCode: true,
                disabled: true
            });
        }

        var n = {};
        this.result();
    },
    result: function(t) {
        if (getApp().globalData.token != null && getApp().globalData.token.length > 0) {
            return;
        }
        wx.showLoading({
            title: "登陆中..."
        });
        e.userlogin(this.loginResult);
    },
    onShow: function() {

        1 == t.globalData.opsetingState && (wx.showLoading({
            title: "授权判断"
        }), wx.getSetting({
            success: function(e) {
                console.log("authenSetting", e);
                e.authSetting["scope.camera"] && e.authSetting["scope.record"] && e.authSetting["scope.userLocation"] && o.setData({
                    opsetingState: 0,
                    hiddenOpenSeting: true
                });
            },
            complete: function() {
                wx.hideToast();
            }
        }));
    },
    getphonenumber: function(t) {
        console.log("getphonenumber", t);
        if (-1 === t.detail.errMsg.indexOf("ok")) {
            wx.hideLoading();
            o.setData({
                focusName: true,
                hiddenTell: false
            });
        }
        else {
            // 获取到电话
            wx.showLoading({
                title: "提交中..."
            });

            wx.login({
                success: function(a) {
                    if (a.code) {
                        var n = {
                            code: a.code,
                            encryptedData: t.detail.encryptedData,
                            iv: t.detail.iv
                        };
                        console.log("phoneCode", a);
                        e.request_post("/api/wxPhoneBind", n, o.userCodeResult, false)
                        // e.request_post("/Callpolice/login/login", n, o.userCodeResult);
                    }
                }
            });
        }
    },
    userCodeResult: function(obj) {
        console.log(obj);
        wx.hideLoading();

        if(0 === obj.code) {
            getApp().globalData.userInfo = obj.user;
            if (obj.user.mobile != null && obj.user.mobile.length > 0 && obj.user.realname != null && obj.user.realname.length > 0) {
                getApp().globalData.needPhone = false;
                o.setData({
                    hiddenGetCode: true,
                    disabled: true
                });
            }
            else if (obj.user.mobile != null && obj.user.mobile.length > 0 ) {
                o.setData({
                    focusName: true,
                    focusInput: false,
                    hiddenTell: false,
                    code:obj.user.mobile,
                    name:""
                });

            }
        }
        else {
            o.setData({
                hiddenGetCode: false,
                disabled: false
            });
        }
    },
    loginResult: function(ss) {
        // console.log(e);

        console.log("loginRe", ss);
        if (null != ss.code && ss.code === 0) {
            // 存储token
            getApp().globalData.token = ss.data.token;
            e.request_get("/api/userInfo", {}, o.userInfoResult, false);
            console.log("loginSucess", getApp().globalData);
        }
        else {
            // 登录失败
            o.setData({
                hiddenGetCode: false,
                disabled: false
            });
            wx.showToast({
                title: ss.msg,
                duration: 2e3,
                image: "/utils/icon/error.png"
            });
        }
    },
    userInfoResult: function(obj) {
        console.log("userInfoResult", obj)
        if (null != obj.code && obj.code === 0) {
            wx.hideLoading();
            getApp().globalData.userInfo = obj.user;
            if (obj.user.mobile != null && obj.user.mobile.length > 0 && obj.user.realname != null && obj.user.realname.length > 0) {
                getApp().globalData.needPhone = false;
                o.setData({
                    hiddenGetCode: true,
                    disabled: true
                });
            }
            else if (obj.user.mobile != null && obj.user.mobile.length > 0 ) {
                o.setData({
                    focusName: true,
                    focusInput: false,
                    hiddenTell: false,
                    code:obj.user.mobile,
                    name:""
                });

            }
        }
        else {
            o.setData({
                hiddenGetCode: false,
                disabled: false
            });
            wx.showToast({
                title: obj.msg,
                duration: 2e3,
                image: "/utils/icon/error.png"
            });
        }
    },
    tellInput: function(e) {
        o.data.code = e.detail.value, 11 == e.detail.value.length && (/^1[34578]\d{9}$/.test(e.detail.value) || wx.showToast({
            title: "电话号码错误",
            duration: 2e3,
            image: "/utils/icon/error.png"
        }));
    },
    nameInput: function(e) {
        o.data.name = e.detail.value;
    },
    confirmName: function() {
        o.setData({
            focusName: !1,
            focusInput: !0
        });
    },
    submitTell: function() {
        var t = o.data.code, a = o.data.name;

        if (11 == t.length && /^1[34578]\d{9}$/.test(t)) {
            var n = {
                mobile: t,
                realName: a
            };
            // console.log(n);
            e.request_post("/api/changeUserInfo", n, o.submitResult);
        } else wx.showToast({
            title: "电话号码错误",
            duration: 2e3,
            image: "/utils/icon/error.png"
        });
        o.setData({
            focusInput: !0
        });
    },
    submitResult: function(e) {
        console.log("submitResult", e);
        if (e.code == 0) {
            wx.showToast({
                title: "提交成功",
                duration: 2e3
            });
            wx.navigateTo({
                url: "../write/write"
            })
            o.setData({
                hiddenGetCode: true
            })
        }
        else {
            wx.showToast({
                title: "提交失败",
                duration: 2e3,
                image: "/utils/icon/error.png"
            })
            o.setData({
                hiddenTell: !0,
                code: "",
                name: ""
            })
        }
    },
    closeToast: function() {
        o.setData({
            hiddenTell: !0,
            code: "",
            name: ""
        });
    },
    callNumber: function() {
        wx.makePhoneCall({
            phoneNumber: "110"
        });
    },
    videoPolice: function() {
/*        wx.navigateTo({
            url: "../webrtc-room/index/index?state=0"
        });*/
        // 视频报警
        // wx.navigateTo({
        //     url: "../video/video?state=0"
        // });
/*        wx.showToast({
            title: "开发中",
            duration: 2e3,
            image: "/utils/icon/error.png"
        });*/
        var self = this;
        self.setData({
            roomNo: "376844",
        });
        console.log("dsfsdf",this.data.users[1]);
        this.joinRoom()
    },
    mutedPolice: function() {
        wx.navigateTo({
            url: "../write/write"
        });
    },
    onReady: function() {},
    onShareAppMessage: function() {
        return {
            imageUrl: "https://www.3bridgetec.cn:8443/api/img/gyshare.png",
            path: "/pages/index/index"
        };
    },




    //tecent cloud
    // 绑定输房间号入框
    bindRoomNo: function (e) {
        console.log("test" ,e);
        var self = this;
        self.setData({
            roomNo: e.detail.value
        });
    },
    userChange: function (e) {
        this.setData({
            index: e.detail.value
        })
    },
    radioChange: function (e) {
        this.setData({
            template: e.detail.value
        });
        console.log('this.data.template', this.data.template)
    },


    // 进入rtcroom页面
    joinRoom: function () {

        var self = this;
        // 防止两次点击操作间隔太快
        var nowTime = new Date();
        if (nowTime - this.data.tapTime < 1000) {
            return;
        }

        if (!self.data.roomNo) {
            wx.showToast({
                title: '请输入房间号',
                icon: 'none',
                duration: 2000
            });
            return
        }

        if (/^\d\d+$/.test(self.data.roomNo) === false) {
            wx.showToast({
                title: '只能为数字',
                icon: 'none',
                duration: 2000
            });
            return
        }

        //var userID = this.data.users[this.data.index]['userId'];
        //var userSig = this.data.users[this.data.index]['userToken'];
        var userID = this.data.users[1]['userId'];
        var userSig = this.data.users[1]['userToken'];

        var url = `../webrtc-room/room/room?roomID=${self.data.roomNo}&template=${self.data.template}&userId=${userID}&userSig=${userSig}`;

        wx.navigateTo({
            url: url
        });

        wx.showToast({
            title: '进入房间',
            icon: 'success',
            duration: 1000
        });

        self.setData({
            'tapTime': nowTime
        });
    },




});