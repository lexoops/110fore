var t = require("../../utils/function.js"), e = "";

Page({
    data: {
        getText: "获取验证码",
        getCss: "btn-def",
        isGetCode: !1,
        phoneDisabled: !1,
        name: "",
        phone: "",
        code: ""
    },
    onLoad: function(t) {
        e = this;
    },
    inputName: function(t) {
        e.data.name = t.detail.value;
    },
    inputPhone: function(t) {
        e.data.phone = t.detail.value, 11 == t.detail.value.length ? (e.data.isGetCode = !0, 
        e.setData({
            getCss: "btn-elect"
        })) : (e.data.isGetCode = !1, e.setData({
            getCss: "btn-def"
        }));
    },
    inputCode: function(t) {
        e.data.code = t.detail.value;
    },
    getCode: function() {
        if (e.data.isGetCode) {
            var a = {
                phone: e.data.phone
            };
            t.request_post("/Callpolice/view/sendsms", a, e.getcoderesult), e.data.isGetCode = !1, 
            e.setData({
                phoneDisabled: !0
            });
            var o = 60, n = setInterval(function() {
                o--, e.setData({
                    getText: "重新获取" + o + "s",
                    getCss: "btn-def"
                }), 0 == o && (clearInterval(n), e.data.isGetCode = !0, e.setData({
                    phoneDisabled: !1,
                    getText: "获取验证码",
                    getCss: "btn-elect"
                }));
            }, 1e3);
        } else t.fail_toast("手机号错误");
    },
    getcoderesult: function(e) {
        1 == e.state ? t.success_toast(e.info) : t.fail_toast(e.info);
    },
    login: function() {
        wx.getLocation({
            success: function(t) {
                console.log(t);
            }
        }), 0 != e.data.name.length ? 11 == e.data.phone.length ? 4 == e.data.code.length ? e.uploadData() : t.fail_toast("验证码错误") : t.fail_toast("手机号码错误") : t.fail_toast("姓名不能为空");
    },
    uploadData: function() {
        wx.getLocation({
            success: function(a) {
                var o = {
                    fullname: e.data.name,
                    phone: e.data.phone,
                    smscode: e.data.code,
                    lat: a.latitude,
                    lng: a.longitude
                };
                t.request_post("/Callpolice/view/binduser", o, e.uploadDataResult);
            },
            fail: function() {
                var a = {
                    fullname: e.data.name,
                    phone: e.data.phone,
                    smscode: e.data.code,
                    lat: "",
                    lng: ""
                };
                t.request_post("/Callpolice/view/binduser", a, e.uploadDataResult);
            }
        });
    },
    uploadDataResult: function(e) {
        1 == e.state ? (t.success_toast(e.info), setTimeout(function() {
            wx.redirectTo({
                url: "../orderdetail/orderdetail"
            });
        }, 1500)) : t.fail_toast(e.info);
    }
});